import React, { useState } from 'react'

const Form = () => {
  const [userRegistration, setUserRegistration] = useState({
    name: '',
    email: '',
    birthday: '',
    age: '',
    gender: '',
    password: ''
  })

  const [records, setRecords] = useState([])
  const handleChange = (e) => {
    const name = e.target.name
    const value = e.target.value

    setUserRegistration({ ...userRegistration, [name]: value })
  }

  const handleSubmit = (e) => {
    const newRecord = {
      ...userRegistration
    }
    setRecords([...records, newRecord])

    setUserRegistration({
      name: '',
      age: '',
      email: '',
      password: '',
      birthday: '',
      gender: ''
    })

    e.preventDefault()
  }

  const dataShow = records.map((value, index) => {
    console.log(value)
    return (
      <tr key={index}>
        <td>{value.name}</td>
        <td>{value.email}</td>
        <td>{value.birthday}</td>
        <td>{value.age}</td>
        <td>{value.gender}</td>
      </tr>
    )
  })
  console.log('datashow', dataShow)
  return (
    <>
      <div className="container">
        <div className="title">
          <h1>Registration Form</h1>
        </div>
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className="name">
            <label
              htmlFor="userName"
            >
              userName
            </label>
            <br />
            <input type="text" name="name"
              id="name"
              onChange={(e) => handleChange(e)}
              required={true}/>
          </div>
          <div className="Email">
            <label htmlFor="Email">Email</label>
            <br />
            <input
              type="email"
              name="email"
              onChange={(e) => handleChange(e)}
              required={true}
            />
          </div>
          <div className="dob">
            <label htmlFor="Dob">Birthday</label>
            <br />
            <input
              type="date"
              name="birthday"
              onChange={(e) => handleChange(e)}
              required={true}
            />
          </div>
          <div className="Age">
            <label htmlFor="age">Age</label>
            <br />
            <input
              type="text"
              name="age"
              onChange={(e) => handleChange(e)}
              required={true}
            />
          </div>
          <div className="Password">
            <label htmlFor="Password">password</label>
            <br />
            <input
              type="password"
              name="password"
              onChange={(e) => handleChange(e)}
              required={true}
            />
          </div>
          <div className="gender">
            <label htmlFor="Gender">gender</label>
            <br />
            <div className="gender-input">
              <input type="radio" value="male" name="gender" onChange={(e) => handleChange(e)}
              required={true} />
              <span>Male</span>
              <input type="radio" value="Female" name="gender" onChange={(e) => handleChange(e)}
              required={true} />
              <span>female</span>
            </div>
          </div>
          <div className="btn">
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>

      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>UserName</th>
              <th>Email</th>
              <th>BirthDay</th>
              <th>Age</th>
              <th>Gender</th>
            </tr>
          </thead>
          <tbody>{dataShow}</tbody>
        </table>
      </div>
    </>
  )
}

export default Form
